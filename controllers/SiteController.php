<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\User;
use app\models\ContactForm;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

	public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
			die('isgest');
            return $this->goHome();
        }

	$model = new User();
	$model->setScenario('login');

	if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if($user = User::findOne(['email' => $model->email])) {
                if($user->validatePassword($model->password)) {
                    Yii::$app->user->login($user);
                    return $this->goBack();
                }
            }

            $model->addError('email', 'User with specified credentials not found.');

        }

        return $this->render('login', [
            'model' => $model,
        ]);
    }

	public function actionRegister()
	{
		$user = new User();
		$user->setScenario('register');
		
//		echo '<pre>';
//		var_dump($user);
//		echo '</pre>';

		if ($user->load(\Yii::$app->request->post()) && $user->validate()) {
			$user->passwordHash = Yii::$app->security->generatePasswordHash($user->password);
//			echo '<pre>';
//			var_dump($user);
//			echo '</pre>';
//			die ($user);
			$user->save();
			\Yii::$app->user->login($user);
			return $this->goHome();
		}

		return $this->render('register', [
			'model' => $user,
		]);
	}

	public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function actionAbout()
    {
        return $this->render('about');
    }
}