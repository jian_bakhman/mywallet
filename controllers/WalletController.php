<?php

namespace app\controllers;

use Yii;
use yii\db;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\CashFlow;
use yii\data\ActiveDataProvider;

class WalletController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['wallet'],
                'rules' => [
                    [
                        'actions' => ['wallet'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

	public function actionAddCash()
    {
	$cash = new CashFlow();

//        $cash->setScenario('createCashFlow');

	if ($cash->load(Yii::$app->request->post()) && $cash->validate()) {
		$cash->userId = Yii::$app->user->id;
		$cash->createdAt = date('Y-m-d H:i:s');
		$cash->save();
		return $this->redirect(['wallet/cashflow']);
	}

	return $this->render('addCash', [
			'model' => $cash,
		]);
    }

    public function actionCashflow()
    {
	$id = Yii::$app->user->id;
	$dp = new ActiveDataProvider([
		'query' => CashFlow::find()->where(['userId' => $id,]),
		'pagination' => [
			'pageSize' => 5,
		],
	]);
	
	$incomes = CashFlow::find()->where(['userId' => $id,])->andWhere(['type' => 'income'])->sum('value');
	$expences = CashFlow::find()->where(['userId' => $id,])->andWhere(['type' => 'expense'])->sum('value');
	$totalAmount = $incomes - $expences;

        return $this->render('cashflow', [
			'dp' => $dp,
			'incomes' => $incomes,
			'expences' => $expences,
			'totalAmount' => $totalAmount,
		]);
    }

}