<?php

use yii\db\Schema;
use yii\db\Migration;

class m151103_212713_user extends Migration
{
    public function up()
    {
	$columns = [
		'id'			 => Schema::TYPE_PK,
		'email'		 => Schema::TYPE_STRING . '(128) NOT NULL',
		'firstName'		 => Schema::TYPE_STRING . '(128) NOT NULL',
		'lastName'		 => Schema::TYPE_STRING . '(128) NOT NULL',
		'passwordHash'	 => Schema::TYPE_STRING . '(128) NOT NULL',
		'authKey'		 => Schema::TYPE_STRING . '(128) NULL',
		'createdAt'		 => Schema::TYPE_DATETIME . ' NOT NULL',
		'updatedAt'		 => Schema::TYPE_DATETIME . ' NOT NULL',
		'deletedAt'		 => Schema::TYPE_DATETIME . ' NULL',
	];
	$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
	$this->createTable('user', $columns, $tableOptions);

	$this->insert('user', [
		'email'		 => 'admin@admin.com',
		'firstName'		 => 'Peter',
		'lastName'		 => 'Peterson',
		'createdAt'		 => date('Y-m-d H:i:s'),
		'updatedAt'		 => date('Y-m-d H:i:s'),
		'passwordHash'	 => Yii::$app->security->generatePasswordHash('123'),
	]);
    }

    public function down()
    {
		$this->dropTable('user');
    }

}
