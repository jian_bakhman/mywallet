<?php

use yii\db\Schema;
use yii\db\Migration;

class m151108_143650_cashflow extends Migration
{
    public function up()
    {
	$columns = [
		'id'			 => Schema::TYPE_PK . ' NOT NULL',
		'userId'		 => Schema::TYPE_INTEGER . ' NOT NULL',
		'value'		 => Schema::TYPE_FLOAT . ' NOT NULL',
		'type'			 => ' ENUM(\'income\', \'expense\') NOT NULL',
		'description'	 => Schema::TYPE_TEXT . ' NOT NULL',
		'createdAt'		 => Schema::TYPE_DATETIME . ' NOT NULL',
	];
	$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
	$this->createTable('cashFlow', $columns, $tableOptions);

	$this->insert('cashFlow', [
		'userId'		 => 1,
		'value'		 => 1000.5,
		'type'			 => 'income',
		'description'	 => 'salary',
		'createdAt'		 => date('Y-m-d H:i:s'),
	]);

	$this->insert('cashFlow', [
		'userId'		 => 1,
		'value'		 => 111.5,
		'type'			 => 'expense',
		'description'	 => 'food',
		'createdAt'		 => date('Y-m-d H:i:s'),
	]);
    }

    public function down()
    {
        $this->dropTable('cashFlow');
    }
}
