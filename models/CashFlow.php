<?php

namespace app\models;

use yii;
use yii\db\ActiveRecord;

class CashFlow extends ActiveRecord
{	
	public static function tableName()
	{
		return 'cashFlow';
	}

	public function rules()
	{
		return [
			[['value', 'type', 'description'], 'required',],
			['type',  'in', 'range'=>['income', 'expense'],],
			['value', 'integer', 'integerOnly' => false,],
		];
	}
}