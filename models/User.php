<?php

namespace app\models;

use yii;
use yii\db\ActiveRecord;

class User extends ActiveRecord implements \yii\web\IdentityInterface
{
	public $password;
	public $passwordRepetition;
	public $rememberMe = true;

	public static function tableName()
	{
		return 'user';
	}

	public function rules()
	{
		return [
			[['email', 'password',], 'required', 'on' => ['login', 'register'],],
			[['firstName', 'lastName', 'passwordRepetition'], 'required', 'on' => 'register',],
			['email', 'email', 'on' => ['login', 'register'],],
			['email', 'unique', 'on' => 'register',],
			['passwordRepetition', 'compare', 'compareAttribute' => 'password', 'on' => 'register',],
		];
	}

     /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
		 return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
	return null;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->passwordHash);
    }
}
