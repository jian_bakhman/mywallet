<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
//use yii\bootstrap\ActiveForm;

$this->title = 'Registration';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields to register:</p>

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-2 control-label'],
        ],
    ]);

	echo $form->field($model, 'email');
	echo $form->field($model, 'firstName');
	echo $form->field($model, 'lastName');
	echo $form->field($model, 'password')->passwordInput();
	echo $form->field($model, 'passwordRepetition')->passwordInput();
	?>
	<div class="form-group">
             <div class="col-lg-offset-2 col-lg-11">
		<?=  Html::submitButton('Registration', ['class' => 'btn btn-primary']); ?>
             </div>
	</div>
	<?php ActiveForm::end(); ?>

</div>