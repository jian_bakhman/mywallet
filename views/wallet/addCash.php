<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Cashflow';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields to cashflow:</p>

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-2 control-label'],
        ],
    ]);

	echo $form->field($model, 'type')->radioList(['income'=>'Income', 'expense'=>'Expense']);
	echo $form->field($model, 'value');
	echo $form->field($model, 'description');

	?>

	<div class="form-group">
             <div class="col-lg-offset-2 col-lg-11">
		<?=  Html::submitButton('Submit', ['class' => 'btn btn-primary']); ?>
             </div>
	</div>

	<?php ActiveForm::end(); ?>

</div>