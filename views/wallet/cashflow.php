<?php

use yii\grid\GridView;

echo 'Incomes:         ', round($incomes, 2), '<br>';
echo 'Expences:       ', round($expences, 2), '<br>';
echo 'Total amount: ', round($totalAmount, 2), '<br>';
echo '<br>';
echo GridView::widget([
	'dataProvider' => $dp,
	'filterModel' => $searchModel,
	'columns' => ['id',
		'type',
		'value',
		'description',
		'createdAt' => [
			'attribute' => 'createdAt',
			'value' => function($model){
				$d = new DateTime($model->createdAt);
				return $d->format('d.m.Y H:i');
			}
		]]
]);